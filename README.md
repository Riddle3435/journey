# Journey
[[_TOC_]]

## Setup

There are three different ways to continue. First running the game immediately, setting it up with the sole purpose of running the game\
or building the environment in order to continue development and/or run the tests.

The pre-built binary files are available for **Windows** and **Linux**. Working with the source is only described for **Linux**.

### Running immediately
Pre-built binary releases of the project for **Windows** and **Linux** can be found in the [`releases`](releases/1.0.0) folder of the repository.

### Build the environment for playing the game
For regular use, these following instructions are enough.\
This is also encapsulated in a convenience shell script called `build_virtual_env.sh`.

First, make sure you have Python 3.8 installed.\
Then we have to setup the virtual environment and install the dependencies as follows:
```bash
python3 -m venv env
. env/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
python3 setup.py develop
```

### Development and Testing
For development work, including running the tests and generating a coverage report, some extra dependencies are needed.

#### After previous setup
After completing the steps outlined above in the section [Build the environment for playing the game](#Build-the-environment-for-playing-the-game), some development dependencies still need to be installed.\
Assuming the virtual environment is active, this can be done as follows:
```bash
python3 -m pip install -r requirements_dev.txt
```

#### First time setting up
To make the setup a bit faster, a script has been created. This script can be run, skipping the need for the instructions in the section [Build the environment for playing the game](#Build-the-environment-for-playing-the-game).\
Execute the script as follows:
```bash
./build_virtual_env.sh
```
After this script has run, activate the virtual environment:
```bash
. env/bin/activate
```
Everything is now setup and ready to go!

## Starting the Game

It is possible to start the game using the binary, as described [here](Running-immediately).\
Otherwise, run main.py located in the journey directory with python 3.
```bash
python3 journey/main.py
```
Note: make sure your current working directory is the root of this repository.

## Game
![Screenshot game with menu.](images/screenshot_menu.png "Welcome to the game!")

### Game Controls
Explanation on how to use the controls in the game.

#### Menu
- Use the **up** and **down** arrows to go through the menu items.
- Confirm using the **Enter** key.
- To open the menu while in the game, press **ESC**.

#### Gameplay
- Use the arrow keys to move **left** and **right**.
- Press **SPACE** to jump.

### Game Win Condition
- Collect all the letters scattered across the levels.
- Make it through the end of the game and gain access to the motivation letter!

Note: You cannot continue to the next level before collecting the page on the current level.

## Running pytest

When it has been setup for development work, the tests can be run as described below.

Run all tests.
```bash
pytest
```

Run all the tests, except the ones marked "slow".
```bash
pytest -m "not slow"
```

This script runs the tests using pytest, while generating a coverage report in HTML.
```bash
./generate_coverage_report.sh
```

## Credits

* Art commissioned from [ThePaintedDogg](https://www.deviantart.com/thepainteddogg)
* Paper sound effect by gynation, found [here](https://freesound.org/people/gynation/sounds/82378/)
