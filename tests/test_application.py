from journey.character import Character
from journey.constants import FPS_DESIRED
from journey import main as mod_main
from journey.main import Application
from pygame.locals import KEYDOWN, K_LEFT, QUIT, K_ESCAPE, K_RETURN, K_KP_ENTER
from tests.utils import FalseAfter, MeasureFPS
import pygame
import pytest


# Background: Running Application, with the menu open.
@pytest.fixture
def app():
    return Application()


# Background: Running Application, without the menu open.
@pytest.fixture
def app_nomenu():
    app = Application()
    app.menu.disable()
    return app


# ID: 001
# Scenario: Verify the quit event stops the game loop.
# Given: the game is in progress
# When: the QUIT event is called
# Then: the game loop stops
def test_001_event_quit(app_nomenu):
    event_list = [pygame.event.Event(QUIT)]
    app_nomenu.process_events(event_list)
    assert app_nomenu.running is False


# ID: 002
# Scenario: Verify that the menu can be opened from the game.
# Given: the game is in progress
# When: the Escape button is pressed
# Then: the menu is opened
def test_002_event_escape(app_nomenu):
    event_list = [pygame.event.Event(KEYDOWN, key=K_ESCAPE)]
    app_nomenu.process_events(event_list)
    assert app_nomenu.menu.is_enabled() is True


# ID: 003
# Scenario: Start with an enable menu, disable the menu using keys.
# Note: While running once through the game loop.
# Given: the menu is enabled
# When: pressed Enter
# Then: the menu is disabled
@pytest.mark.parametrize("event, key", [
    pytest.param(KEYDOWN, K_RETURN, id="Return"),
    pytest.param(KEYDOWN, K_KP_ENTER, id="Keypad Enter")
])
def test_003_enable_disable_menu(mocker, app, event, key):
    def get_events():
        event_list = [pygame.event.Event(event, key=key)]
        return event_list
    mocker.patch('journey.main.Application.status_running', FalseAfter(1))
    mocker.patch('pygame.event.get', get_events)
    app.run_application()
    assert app.menu.is_enabled() is False


# ID: 004
# Scenario: Start with a disabled menu, enable menu using keys.
# Note: While running once through the game loop.
# Given: the menu is disabled
# When: pressed Escape
# Then: the menu is enabled
def test_004_disable_enable_menu(mocker, app_nomenu):
    def get_events():
        event_list = [pygame.event.Event(KEYDOWN, key=K_ESCAPE)]
        return event_list
    mocker.patch('journey.main.Application.status_running', FalseAfter(1))
    mocker.patch('pygame.event.get', get_events)
    app_nomenu.run_application()
    assert app_nomenu.menu.is_enabled() is True


# ID: 005
# Scenario: When is application is started, the application loop is running.
# Given: the application is started
# When: the initialization is finished
# Then: the game loop is runnig
def test_005_application_running_when_init(app):
    assert app.running is True


# ID: 006
# Scenario: Verify pygame initialization went well.
# Given: the application is started
# When: the application has been initialized
# Then: pygame has been initialized
def test_006_pygame_init():
    _ = Application()
    assert pygame.get_init() is True


# ID: 007
# Scenario: Character has been initialized with application start.
# Given: the application has started
# When: the application has been initialized
# Then: the character has been created
@pytest.mark.componentintegration
def test_007_application_init_character(app):
    assert app.stage.character is not None


# ID: 008
# Scenario: When the left key is pressed, the character moves to the left.
# Given: the application is running without the menu open
# When: the left key is pressed
# Then: the character moves to the left
@pytest.mark.componentintegration
def test_008_event_keydown_left_character(app_nomenu):
    event_list = [pygame.event.Event(KEYDOWN, key=K_LEFT)]
    app_nomenu.process_events(event_list)
    assert app_nomenu.stage.character.dx == -Character.SPEED


# ID: 009
# Scenario: When a move key is pressed, while the menu is open,
#           the character doesn't move.
# Given: the application is running with the menu open
# When: a move key is pressed
# Then: the character doesn't move
@pytest.mark.componentintegration
def test_009_event_menu_keydown_left_character(app, mocker):
    def get_events():
        event_list = [pygame.event.Event(KEYDOWN, key=K_LEFT)]
        return event_list
    mocker.patch('journey.main.Application.status_running', FalseAfter(1))
    mocker.patch('pygame.event.get', get_events)
    app.run_application()
    assert app.stage.character.dx == 0


# ID: 010
# Scenario: Verify titel window.
# Given: the application is started
# When: the title is set
# Then: the title of the project match requirement
def test_010_application_title_window():
    _ = Application()
    title = pygame.display.get_caption()[0]
    assert title is not None
    assert title == "Journey"


# ID: 011
# Scenario: When main is called, application needs to start.
# Given: a global var set to 0 AND main is called
# When: this global value is changed in application
# Then: this global value is adjusted accordingly
def test_011_mod_main(mocker):
    global testvar
    testvar = 0

    def setglobal(_):
        global testvar
        testvar = 0xC0FFEE

    print(f"\n{testvar}")
    assert testvar == 0
    mocker.patch.object(Application, 'run_application', setglobal)
    mocker.patch.object(mod_main, "__name__", "__main__")
    mod_main.main()
    print(testvar)
    assert testvar == 0xC0FFEE


# ID: 012
# Scenario: Verify that the FPS matches the desired FPS.
# Given: a desired FPS of 40 FPS
# When: the current FPS is measured
# Then: the FPS matches the desired FPS with a maximum of 2 FPS less
@pytest.mark.slow
def test_012_fps(mocker, app):
    fps = mocker.patch(
        'journey.main.Application.status_running',
        MeasureFPS(seconds=5)
    )
    app.run_application()
    print(f"\nFPS: {fps.get_value()}\nFPS rounded: {round(fps.get_value())}")
    assert round(fps.get_value()) >= FPS_DESIRED - 2
