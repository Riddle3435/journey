from journey.constants import START_POS_Y, RENDER_WIDTH
from journey.character import Character
from journey.stage import Stage
from journey.main import Application
from journey.sprite import (
    StopLight, Letter,
    Solid, CloudPlatform,
    RedRobot, YellowRobot,
    CarRobot, DrunkenHorseRobot
)
from tests.utils import FalseAfter
import pytest


# Background: Running Application, with the menu open.
@pytest.fixture
def app():
    return Application()


# Background: Creating a stage with a letter and stoplight.
@pytest.fixture
def stage():
    character = Character()
    stoplight = StopLight()
    letter = Letter()
    red_robot = RedRobot()
    yellow_robot = YellowRobot()

    teststage = Stage(
        background='gfx/scene_1.png',
        character=character,
        platforms=[
            Solid(0, START_POS_Y + Character.HEIGHT,
                  RENDER_WIDTH, 100),
        ],
        items=[
            red_robot,
            yellow_robot,
            CarRobot(),
            DrunkenHorseRobot(),
            letter,
            stoplight
        ]
    )
    teststage.stoplight = stoplight
    return teststage


# ID: 301
# Scenario: The stoplight starts with a red light.
# Given: a stoplight is made
# When: no state has been changed
# Then: the stoplight is red
def test_301_stoplight_start_colour(app):
    stoplight = StopLight()
    assert stoplight.state is stoplight.STATE_RED


# ID: 302
# Scenario: The stoplight can change from red to green.
# Given: a stoplight is made
# When: the state is changed
# Then: the stoplight switches colour
@pytest.mark.parametrize("state", [
    pytest.param(StopLight.STATE_GREEN, id="State Green"),
    pytest.param(StopLight.STATE_YELLOW, id="State Yellow"),
    pytest.param(StopLight.STATE_RED, id="State Red"),
    pytest.param(StopLight.STATE_OFF, id="State Off")
])
def test_302_stoplight_colour_change(app, state):
    stoplight = StopLight()
    stoplight.state = state
    assert stoplight.state is state


# ID: 303
# Scenario: Picking up the letter, changes the stoplight.
# Given: the application is run
# When: the letter is hit
# Then: the stoplight changes state to green
@pytest.mark.componentintegration
def test_303_letter_change_stoplight_state(app, stage, mocker):
    mocker.patch('journey.main.Application.status_running', FalseAfter(1))
    mocker.patch('pygame.sprite.spritecollideany', return_value=Letter(0, 0))

    app.stage = stage
    app.run_application()
    assert stage.stoplight.state is StopLight.STATE_GREEN


# ID: 304
# Scenario: Hitting only a green stoplight results in going to the next stage.
# Given: the stoplight is green
# When: the stoplight is hit
# Then: going to next stage
@pytest.mark.componentintegration
@pytest.mark.parametrize("state, index", [
    pytest.param(StopLight.STATE_GREEN, 1, id="State Green"),
    pytest.param(StopLight.STATE_RED, 0, id="State Red")
])
def test_304_green_stoplight_next_stage(app, stage, mocker, state, index):
    mocker.patch('journey.main.Application.status_running', FalseAfter(1))
    teststoplight = StopLight()
    teststoplight.state = state
    mocker.patch('pygame.sprite.spritecollideany', return_value=teststoplight)

    app.stage = stage
    app.run_application()
    assert app.stage_index == index


# ID: 305
# Scenario: When finished the game, the game over menu shows.
# Given: the last stage is used and the stoplight is green
# When: hitting the stoplight
# Then: the game over menu is shown
def test_305_game_over(app, stage, mocker):
    mocker.patch('journey.main.Application.status_running', FalseAfter(1))
    green_light = StopLight()
    green_light.state = StopLight.STATE_GREEN
    mocker.patch('pygame.sprite.spritecollideany', return_value=green_light)

    app.stage = stage
    app.stage_index = 3
    app.run_application()
    assert app.menu.is_enabled() is True
