from tests.utils import FalseAfter, ErrorAfter, CallableExhausted
import pytest


# ID: 201
# Scenario: The utils "running loop for x times" works as expected.
# Given: the false after utils is run
# When: received the requested amount
# Then: it will run for the expected amount
def test_201_false_after():
    fa = FalseAfter(1)
    assert fa() is True
    assert fa() is False


# ID: 202
# Scenario: The utils "error after running loop for x times" works as expected.
# Given: the error after is run
# When: received the requested amount
# Then: it will throw an error after the expected amount
def test_202_error_after():
    ea = ErrorAfter(1)
    assert ea() is None
    pytest.raises(CallableExhausted, ea)
