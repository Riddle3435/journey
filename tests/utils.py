from datetime import datetime


class FalseAfter(object):
    def __init__(self, limit):
        self.limit = limit
        self.calls = 0

    def __call__(self):
        self.calls += 1
        return self.calls <= self.limit


class ErrorAfter(object):
    def __init__(self, limit):
        self.limit = limit
        self.calls = 0

    def __call__(self):
        self.calls += 1
        if self.calls > self.limit:
            raise CallableExhausted


class CallableExhausted(Exception):
    pass


class MeasureFPS(object):
    def __init__(self, seconds=5):
        self.calls = 0
        self.seconds = seconds
        self.start = None
        self.delta = None

    def __call__(self):
        current = datetime.now()
        if self.calls == 0:
            self.start = current
        self.calls += 1
        self.delta = current - self.start
        if self.delta.seconds >= self.seconds:
            return False
        return True

    def get_value(self):
        s = self.delta.seconds + (self.delta.microseconds/1000000)
        return float(self.calls / s)
