from journey.character import Character
from journey.constants import RENDER_WIDTH, RENDER_HEIGHT
from journey.sprite import CloudPlatform, check_collision
from pygame.locals import K_ESCAPE, KEYDOWN, KEYUP, K_LEFT, K_RIGHT, K_SPACE
from tests.constants import T_CHAR_X, T_CHAR_Y
import pygame
import pytest


# ID: 101
# Scenario: When a character is initialized, the x and y coordinates are at 0.
# Given: a character has been created
# When: the x and y position hasn't changed
# Then: the starting value is x=0 and y=0
def test_101_character_init_location():
    char = Character()
    assert char.x == 0 and char.y == 0


# Background: A character has been created a location T_CHAR_X and T_CHAR_Y.
@pytest.fixture
def mychar():
    return Character(T_CHAR_X, T_CHAR_Y)


# ID: 102
# Scenario: When a character has been created with x and y values,
#           this reflects the position of the character.
# Given: a character has been created
# When: x and y position values where given
# Then: the character is created at x and y location
def test_102_character_init_location_set_value(mychar):
    assert mychar.x == T_CHAR_X
    assert mychar.y == T_CHAR_Y


# ID: 103
# Scenario: When the character moves left, right, up or down,
#           this reflects the position.
# Given: the character has been created
# When: the character moves
# Then: the new location matches the movement
@pytest.mark.parametrize("dx, dy, exp_x, exp_y", [
    pytest.param(
        -Character.SPEED, 0,
        T_CHAR_X - Character.SPEED, T_CHAR_Y, id="Move Left"),
    pytest.param(
        Character.SPEED, 0,
        T_CHAR_X + Character.SPEED, T_CHAR_Y, id="Move Right"),
    pytest.param(
        0, -Character.SPEED,
        T_CHAR_X, T_CHAR_Y - Character.SPEED, id="Move Up"),
    pytest.param(
        0, Character.SPEED,
        T_CHAR_X, T_CHAR_Y + Character.SPEED, id="Move Down"),
])
def test_103_character_move_dx_dy(mychar, dx, dy, exp_x, exp_y):
    mychar.dx = dx
    mychar.dy = dy
    mychar.move([])
    assert mychar.x == exp_x
    assert mychar.y == exp_y


# ID: 104
# Scenario: When the keyboard is being used,
#           the horizantal expected movement matches.
# Given: a character has been made
# When: a key is pressed
# Then: the horizantal movement matches
@pytest.mark.parametrize("event, key, dx", [
    pytest.param(KEYDOWN, K_LEFT, -Character.SPEED, id="KeyDown Left"),
    pytest.param(KEYDOWN, K_RIGHT, Character.SPEED, id="KeyDown Right"),
    pytest.param(KEYDOWN, K_ESCAPE, 42, id="KeyDown ESC"),
    pytest.param(KEYUP, K_LEFT, 0, id="KeyUp Left"),
    pytest.param(KEYUP, K_RIGHT, 0, id="KeyUp Right"),
    pytest.param(KEYUP, K_ESCAPE, 42, id="KeyUp ESC"),
    pytest.param(pygame.QUIT, None, 42, id="Quit")
])
def test_104_character_move_key_horizontal(mychar, event, key, dx):
    mychar.dx = 42
    ev_keydown = pygame.event.Event(event, key=key)
    mychar.key_handler(ev_keydown)
    assert mychar.dx == dx


# ID: 105
# Scenario: When the space key is pressed, the character jumps.
# Given: a character has been made
# When: the space key is pressed
# Then: the character jumps
@pytest.mark.parametrize("airtime, v", [
    pytest.param(0, Character.VELOC, id="Jump from the ground"),
    pytest.param(3, Character.VELOC, id="Jump after slight airtime"),
    pytest.param(6, 0, id="Cannot jump after significant airtime"),
])
def test_105_character_state_jump_space_key(mychar, airtime, v):
    mychar.airtime = airtime
    ev_keydown_space = pygame.event.Event(KEYDOWN, key=K_SPACE)
    mychar.key_handler(ev_keydown_space)
    assert mychar.velocity == v


# Background: Helper function for the gravity test.
def grav_test_help(v, m):
    return round(-1*(0.5*m*(v**2)))


# ID: 106
# Scenario: Gravity has been implemented.
# Given: a character has been made
# When: a character jumps
# Then: the jump action follows the curve expected
@pytest.mark.parametrize("v, dy", [
    pytest.param(
        Character.VELOC,
        grav_test_help(Character.VELOC, Character.MASS)),
    pytest.param(1, grav_test_help(1, Character.MASS)),
    pytest.param(0, grav_test_help(0, Character.MASS)),
    pytest.param(-1, grav_test_help(-1, -Character.MASS)),
    pytest.param(
        -Character.VELOC, grav_test_help(-Character.VELOC, -Character.MASS)),
    pytest.param(
        -(Character.VELOC+1),
        grav_test_help(-(Character.VELOC+1), -Character.MASS)),
])
def test_106_character_gravity(v, dy):
    char = Character()
    char.velocity = v+1
    char.gravity()
    assert char.velocity == v

    char.gravity()
    assert char.dy == dy


# ID: 107
# Scenario: A character cannot move outside the screen view.
# Given: a character has been made
# When: the character moves out of the screen view
# Then: the movement action stops before crossing the view border
@pytest.mark.parametrize("dx, dy", [
    pytest.param(-RENDER_WIDTH, 0, id="Check left boundary"),
    pytest.param(RENDER_WIDTH, 0, id="Check right boundary"),
    pytest.param(0, -RENDER_HEIGHT, id="Check top boundary"),
    pytest.param(0, RENDER_HEIGHT, id="Check bottom boundary")
])
def test_107_bounds_field(mychar, dx, dy):
    mychar.dx = dx
    mychar.dy = dy
    mychar.move([])
    assert mychar.x == T_CHAR_X
    assert mychar.y == T_CHAR_Y


# ID: 108
# Scenario: Character animation moves as expected.
# Given: a character is walking
# When: a animation tick passes
# Then: the expected walking state is shown
def test_108_animation_ticks(mychar):
    mychar.state = Character.WALKING
    mychar.animate()
    assert mychar.anim_ticks == 1
    print(f"\nTicks: { mychar.anim_ticks }, Expected: 1 ")
    mychar.anim_ticks = 3
    mychar.animate()
    assert mychar.anim_ticks == 4
    print(f"Ticks: { mychar.anim_ticks }, Expected: 4 ")
    mychar.animate()
    assert mychar.anim_ticks == 0
    print(f"Ticks: { mychar.anim_ticks }, Expected: 0 ")


# ID: 109
# Scenario: Going from one character state to the next is verified.
# Given: a starting character state
# When: dx and/or dy is changed
# Then: the expected next state is set
@pytest.mark.parametrize("state,expected_state,dx,dy", [
    pytest.param(Character.STANDING, Character.STANDING,
                 0, 0, id="STANDING->STANDING dx=0, dy=0"),
    pytest.param(Character.STANDING, Character.WALKING,
                 2, 0, id="STANDING->WALKING dx!=0, dy=0"),
    pytest.param(Character.STANDING, Character.JUMPING,
                 0, -5, id="STANDING->JUMPING dx=?, dy<0"),
    pytest.param(Character.STANDING, Character.FALLING,
                 0, 5, id="STANDING->FALLING dx=?, dy>0"),

    pytest.param(Character.WALKING, Character.WALKING,
                 -2, 0, id="WALKING->WALKING dx!=0, dy=0"),
    pytest.param(Character.WALKING, Character.STANDING,
                 0, 0, id="WALKING->STANDING dx=0, dy=0"),
    pytest.param(Character.WALKING, Character.JUMPING,
                 2, -5, id="WALKING->JUMPING dx!=0, dy<0"),
    pytest.param(Character.WALKING, Character.FALLING,
                 2, 5, id="WALKING->FALLING dx!=0, dy>0"),

    pytest.param(Character.JUMPING, Character.FALLING,
                 0, 5, id="JUMPING->FALLING dx=?, dy>0"),
    pytest.param(Character.JUMPING, Character.JUMPING,
                 0, 0, id="JUMPING->JUMPING dx=?, dy=0"),
    pytest.param(Character.JUMPING, Character.JUMPING,
                 0, -5, id="JUMPING->JUMPING dx=?, dy<0"),

    pytest.param(Character.FALLING, Character.FALLING,
                 0, 5, id="FALLING->FALLING dx=0, dy>0"),
    pytest.param(Character.FALLING, Character.WALKING,
                 2, 0, id="FALLING->WALKING dx!=0, dy=0"),
    pytest.param(Character.FALLING, Character.STANDING,
                 0, 0, id="FALLING->STANDING dx=0, dy=0")
])
def test_109_state_transition(mychar, state, expected_state, dx, dy):
    mychar.state = state
    mychar.dx = dx
    mychar.dy = dy

    mychar.next_state()
    assert mychar.state == expected_state


# ID: 110
# Scenario: If a character hits a cloud, this is detected.
# Given: a character is created
# When: moved into a cloud
# Then: the hit detection will detect it
@pytest.mark.componentintegration
def test_110_collision(mychar):
    plat_clash = CloudPlatform(400, 332)
    assert check_collision(mychar, plat_clash) is True
    plat_no_clash = CloudPlatform(100, 100)
    assert check_collision(mychar, plat_no_clash) is False
