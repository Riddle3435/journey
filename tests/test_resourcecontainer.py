from datetime import datetime
from journey.resources import ResourceContainer
from pygame import Surface
from pygame.mixer import Sound
import pygame
import pytest


# Background: Started the resource container.
@pytest.fixture
def rc():
    return ResourceContainer()


# ID: 401
# Scenario: When the resource container is instantiated two times,
#           the same resource container is returned.
# Given: the resource container is instantiated two times
# When: comparing their memory address
# Then: the memory address matches
def test_401_resourcecontainer_singleton():
    rc1 = ResourceContainer()
    rc2 = ResourceContainer()
    print(f"\nrc1:{id(rc1)}\nrc2:{id(rc2)}")
    assert id(rc1) == id(rc2)


# ID: 402
# Scenario: When a file is queried through the resource container
#           it returns the appropriate type.
# Given: a file is going through the resource container
# When: a file or sound is sent
# Then: the file type is correctly detected and handled
@pytest.mark.parametrize('filename, expected_type, loader', [
    pytest.param(
        'tests/testimage.png',
        Surface,
        'get_image',
        id='get_image->Surface'),
    pytest.param(
        'tests/testsound.ogg',
        Sound,
        'get_sound',
        id='get_sound->Sound'),
])
def test_402_resourcecontainer_produces_resource(
        rc, filename, expected_type, loader):
    pygame.mixer.init()
    object = getattr(rc, loader)(filename)
    assert object is not None
    assert isinstance(object, expected_type)


# ID: 403
# Scenario: Verify that using the resource container decreases
#           the time it takes to load a resource.
# Given: the resource container is used
# When: an image is loaded a second time
# Then: the load time is faster
def test_403_resourcecontainer_loads_faster():
    def time_it(filename):
        start = datetime.now()
        rc = ResourceContainer()
        img = rc.get_image(filename)
        end = datetime.now()
        print(f"\nimg: {id(img)}")
        return end-start

    time_first_load = time_it('tests/testimage_2.png')
    time_second_load = time_it('tests/testimage_2.png')

    print(f"\nFirst load time: {time_first_load},\n"
          f"Second load time: {time_second_load}")
    assert time_second_load < time_first_load


# ID: 404
# Scenario: Verify the calling the resource container with
#           the same resource results in the image has the same id.
# Given: the resource container is used
# When: two identical images are send through
# Then: the id is the same
def test_404_resourcecontainer_gives_same_reference():
    rc = ResourceContainer()
    img1 = rc.get_image('tests/testimage.png')
    img2 = rc.get_image('tests/testimage.png')
    print(f"\nimg1:{id(img1)}\nimg2:{id(img2)}\n")
    assert id(img1) == id(img2)
