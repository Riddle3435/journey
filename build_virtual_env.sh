if [ -d "env" ]; then
    rm -rf env
fi

python3 -m venv env
. env/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
python3 -m pip install -r requirements_dev.txt
python3 setup.py develop
