from journey.resources import ResourceContainer
from pygame import Surface
from pygame.mixer import Sound
import pygame


class JourneySprite(pygame.sprite.DirtySprite):
    def __init__(self, x=0, y=0, w=0, h=0, position=None):
        # Pygame sprite initialization
        pygame.sprite.DirtySprite.__init__(self)
        self.image = pygame.Surface((w, h))
        self.image.set_colorkey((0, 0, 0))
        _x, _y = x, y
        if position is not None:
            _x, _y = position
        self.initial_pos = (_x, _y)
        self.rect = pygame.Rect(_x, _y, w, h)
        self.source_rect = pygame.Rect(0, 0, w, h)
        self.dirty = 2

        # Prepare animation, movement and collision basics
        self.rect_collide = self.rect.copy()
        self.anim_ticks = 0
        self.frame_index = 0
        self.dx = self.dy = 0

    def move(self, dx=0, dy=0):
        self.rect.move_ip(dx, dy)
        self.rect_collide.move_ip(dx, dy)

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, filename_or_surface):
        if isinstance(filename_or_surface, Surface):
            self._image = filename_or_surface
        elif isinstance(filename_or_surface, str):
            rc = ResourceContainer()
            self._image = rc.get_image(filename_or_surface)

    @property
    def sfx(self):
        return self._sfx

    @sfx.setter
    def sfx(self, filename_or_sound):
        if isinstance(filename_or_sound, Sound):
            self._sfx = filename_or_sound
        elif isinstance(filename_or_sound, str):
            rc = ResourceContainer()
            self._sfx = rc.get_sound(filename_or_sound)

    @property
    def x(self):
        return self.rect.x

    @property
    def y(self):
        return self.rect.y

    @x.setter
    def x(self, new_x):
        rc_off = abs(self.rect.x - self.rect_collide.x)
        self.rect.x = new_x
        self.rect_collide.x = new_x + rc_off

    @y.setter
    def y(self, new_y):
        rc_off = abs(self.rect.y - self.rect_collide.y)
        self.rect.y = new_y
        self.rect_collide.y = new_y + rc_off


Solid = JourneySprite


class CloudPlatform(Solid):
    WIDTH = 96
    HEIGHT = 32
    VARIATIONS = 3

    count = 0

    def __init__(self, x=0, y=0, position=None):
        Solid.__init__(self, x, y, __class__.WIDTH, __class__.HEIGHT, position)
        self.image = 'gfx/clouds.png'
        self.source_rect.y = (CloudPlatform.count * __class__.HEIGHT)
        CloudPlatform.count = (CloudPlatform.count + 1) % __class__.VARIATIONS
        # Collision
        self.rect_collide.inflate_ip(
            -0.75*__class__.HEIGHT,
            -0.75 * __class__.HEIGHT)
        self.rect_collide.move_ip(0, 0.25 * __class__.HEIGHT)


class Letter(JourneySprite):
    WIDTH = 32
    HEIGHT = 32
    VERTICAL_SWAY = 5
    ANIM_DIV = 4

    def __init__(self, x=0, y=0, position=None):
        JourneySprite.__init__(self, x, y, __class__.WIDTH,
                               __class__.HEIGHT, position)
        self.image = 'gfx/letter.png'
        self.sfx = 'sfx/paper.ogg'
        self.dy = -1

    def animate(self):
        self.anim_ticks = (self.anim_ticks + 1) % __class__.ANIM_DIV
        if self.anim_ticks == 0:
            self.move(0, self.dy)
            if abs(self.y - self.initial_pos[1]) >= __class__.VERTICAL_SWAY:
                self.dy *= -1

    def update(self, hit=False):
        if hit:
            self.sfx.play()
            self.kill()
        self.animate()


class StopLight(JourneySprite):
    WIDTH = 26
    HEIGHT = 79

    STATE_OFF = 0
    STATE_RED = 1
    STATE_YELLOW = 2
    STATE_GREEN = 3

    def __init__(self, x=0, y=0):
        JourneySprite.__init__(self, x, y, __class__.WIDTH, __class__.HEIGHT)
        self.image = 'gfx/stoplight.png'
        self.state = __class__.STATE_RED
        self.dirty = 2

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new_state):
        self._state = new_state
        self.source_rect.x = self._state * __class__.WIDTH

    def update(self, hit=False):
        pass


class RedRobot(JourneySprite):
    WIDTH = 49
    HEIGHT = 35
    ROW = 2
    ANIM_DIV = 5
    FRAMES = 2

    def __init__(self, x=0, y=0, position=None):
        JourneySprite.__init__(
            self, x, y,
            __class__.WIDTH,
            __class__.HEIGHT,
            position)
        self.image = 'gfx/robots.png'
        self.source_rect.y = __class__.HEIGHT * __class__.ROW

    def animate(self):
        self.anim_ticks = (self.anim_ticks + 1) % __class__.ANIM_DIV
        if self.anim_ticks == 0:
            self.frame_index = (self.frame_index + 1) % __class__.FRAMES
        self.source_rect.x = self.frame_index * __class__.WIDTH

    def update(self, hit=False):
        if not hit:
            self.animate()


class YellowRobot(JourneySprite):
    WIDTH = 49
    HEIGHT = 35
    ROW = 3
    ANIM_DIV = 8
    FRAMES = 2

    def __init__(self, x=0, y=0, position=None):
        JourneySprite.__init__(
            self, x, y,
            __class__.WIDTH,
            __class__.HEIGHT,
            position)
        self.image = 'gfx/robots.png'
        self.source_rect.y = __class__.HEIGHT * __class__.ROW

    def animate(self):
        self.anim_ticks = (self.anim_ticks + 1) % __class__.ANIM_DIV
        if self.anim_ticks == 0:
            self.frame_index = (self.frame_index + 1) % __class__.FRAMES
        self.source_rect.x = self.frame_index * __class__.WIDTH

    def update(self, hit=False):
        if not hit:
            self.animate()


class CarRobot(JourneySprite):
    WIDTH = 49
    HEIGHT = 35
    ROW = 0
    ANIM_DIV = 5
    FRAMES = 11

    def __init__(self, x=0, y=0, position=None):
        JourneySprite.__init__(
            self, x, y,
            __class__.WIDTH,
            __class__.HEIGHT,
            position)
        self.image = 'gfx/robots.png'
        self.source_rect.y = __class__.HEIGHT * __class__.ROW

    def animate(self):
        self.anim_ticks = (self.anim_ticks + 1) % __class__.ANIM_DIV
        if self.anim_ticks == 0:
            self.frame_index = (self.frame_index + 1) % __class__.FRAMES
        self.source_rect.x = self.frame_index * __class__.WIDTH

    def update(self, hit=False):
        if not hit:
            self.animate()


class DrunkenHorseRobot(JourneySprite):
    WIDTH = 49
    HEIGHT = 35
    ROW = 1
    ANIM_DIV = 5
    FRAMES = 8

    def __init__(self, x=0, y=0, position=None):
        JourneySprite.__init__(
            self, x, y,
            __class__.WIDTH,
            __class__.HEIGHT,
            position)
        self.image = 'gfx/robots.png'
        self.source_rect.y = __class__.HEIGHT * __class__.ROW

    def animate(self):
        self.anim_ticks = (self.anim_ticks + 1) % __class__.ANIM_DIV
        if self.anim_ticks == 0:
            self.frame_index = (self.frame_index + 1) % __class__.FRAMES
        self.source_rect.x = self.frame_index * __class__.WIDTH

    def update(self, hit=False):
        if not hit:
            self.animate()


# Compares two sprite for collision
def check_collision(sprite_one, sprite_two):
    rect_one = sprite_one.rect_collide
    rect_two = sprite_two.rect_collide
    return bool(rect_one.colliderect(rect_two))
