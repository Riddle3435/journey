from journey.constants import RENDER_WIDTH, RENDER_HEIGHT, CREDITS
from pygame.locals import KEYDOWN, KEYUP, K_RETURN, K_KP_ENTER
from pygame_menu.baseimage import BaseImage, IMAGE_MODE_SIMPLE
from pygame_menu import events as menu_events
from pygame_menu.menu import Menu
from pygame_menu.themes import Theme
from pygame_menu.locals import ALIGN_LEFT
from pygame_menu.font import FONT_8BIT, FONT_MUNRO, FONT_PT_SERIF
import webbrowser


JOURNEY_THEME = Theme(
    background_color=BaseImage(
        image_path='gfx/menubar.png',
        drawing_mode=IMAGE_MODE_SIMPLE
    ),
    menubar_close_button=False,
    title_background_color=(4, 47, 126),
    title_font=FONT_8BIT,
    title_offset=(20, 10),
    title_shadow=True,
    widget_font_color=(255, 255, 255),
    widget_font=FONT_MUNRO
)


LETTER_THEME = Theme(
    background_color=(255, 255, 255),
    menubar_close_button=False,
    title_background_color=(4, 47, 126),
    title_font=FONT_8BIT,
    title_offset=(0, 0),
    title_shadow=True,
    scrollbar_thick=10,
    selection_color=(0, 0, 0),
    widget_alignment=ALIGN_LEFT,
    widget_font_color=(0, 0, 0),
    widget_font_size=22,
    widget_font=FONT_PT_SERIF
)


class JourneyMenu(object):
    HEIGHT = 0.5 * RENDER_HEIGHT
    WIDTH = 0.5 * RENDER_WIDTH
    CREDITS_HEIGHT = 0.8 * RENDER_HEIGHT
    TITLE = 'Journey'

    def __init__(self):
        self.menu = Menu(
            __class__.HEIGHT, __class__.WIDTH,
            __class__.TITLE, theme=JOURNEY_THEME,
            screen_dimension=(RENDER_WIDTH, RENDER_HEIGHT))

        self.credits = Menu(
            __class__.CREDITS_HEIGHT, __class__.WIDTH,
            __class__.TITLE, theme=JOURNEY_THEME,
            screen_dimension=(RENDER_WIDTH, RENDER_HEIGHT))

        self.endgame = Menu(
            __class__.HEIGHT, __class__.WIDTH,
            __class__.TITLE, theme=JOURNEY_THEME,
            screen_dimension=(RENDER_WIDTH, RENDER_HEIGHT))

        self.has_quit = False

        # Menu
        self.menu.add_button('Play', self.__start_game)
        self.menu.add_text_input(
            'Motivation Letter Code: ', textinput_id='code',
            maxchar=4, onreturn=self.__enter_code)
        self.menu.add_button('Credits', self.credits)
        self.menu.add_button('Quit', self.__quit)

        # Credits
        self.credits.add_vertical_margin(20)
        for linenum, line in enumerate(CREDITS):
            fontsize = (26, 20, 22, 5)[linenum % 4]
            self.credits.add_label(line, font_size=fontsize)
        self.credits.add_vertical_margin(20)
        self.credits.add_button('Back', menu_events.BACK)

        # End Game
        self.endgame.add_label('Congratulations!!!', font_size=40)
        self.endgame.add_label('Code: 3435')
        self.endgame.add_label('Use the code in the main menu', font_size=20)
        self.endgame.add_label('to unlock the motivation letter!',
                               font_size=20)
        self.endgame.add_vertical_margin(20)
        self.endgame.add_button('Main menu', menu_events.RESET)

    def enable(self):
        self.menu.enable()

    def disable(self):
        self.menu.disable()

    def is_enabled(self):
        return self.menu.is_enabled()

    def draw(self, surface):
        if self.menu.is_enabled():
            self.menu.draw(surface)

    def update(self, events):
        for ev in events:
            if ev.type in [KEYDOWN, KEYUP]:
                if ev.key == K_KP_ENTER:
                    ev.key = K_RETURN
        return self.menu._current.update(events)

    def open_endgame(self):
        self.menu.enable()
        self.menu._open(self.endgame)

    def __start_game(self):
        self.menu.disable()

    def __enter_code(self, code):
        # You found it! But why is this such a cool number? ;)
        if code == "3435":
            url = 'https://drive.google.com/file/d/1mMul3e1ClGWEkmtI8Hs2jFf0n7f67SJ-/view'
            webbrowser.open_new(url)

    def __quit(self):
        self.has_quit = True
