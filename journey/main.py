from journey.constants import (
    RENDER_WIDTH, RENDER_HEIGHT,
    FPS_DESIRED
)
from journey.menu import JourneyMenu
from journey.sprite import Letter, check_collision, StopLight
from journey.stages import Stages
from pygame.locals import KEYDOWN, KEYUP, QUIT, K_ESCAPE
import pygame
import sys


DEBUG = False


class Application(object):
    def __init__(self):
        pygame.init()
        self.running = True

        # Window setup
        pygame.display.set_caption("Journey")
        icon = pygame.image.load('gfx/icon.png')
        pygame.display.set_icon(icon)

        aspect_ratio = RENDER_WIDTH / RENDER_HEIGHT
        self.screen_mode = \
            [mode for mode in pygame.display.list_modes()
             if (mode[0] / mode[1]) == aspect_ratio][0]

        self.screen = pygame.display.set_mode(self.screen_mode)
        self.render = pygame.Surface((RENDER_WIDTH, RENDER_HEIGHT))

        # Menu
        self.menu = JourneyMenu()

        # Stages
        s = Stages()
        self.stages = s.get_stages()
        self.stage_index = 0
        self.stage = self.stages[self.stage_index]

        self.clock = pygame.time.Clock()

    def process_events(self, event_list):
        for event in event_list:
            if event.type == QUIT:
                self.running = False
            elif event.type == KEYDOWN or event.type == KEYUP:
                if event.key == K_ESCAPE:
                    self.menu.enable()
                    self.stage.character.dx = 0
                self.stage.character.key_handler(event)

    def next_stage(self):
        self.stage_index += 1
        if self.stage_index < len(self.stages):
            self.stage = self.stages[self.stage_index]
        else:
            self.menu.open_endgame()
            self.stage.character.x = RENDER_WIDTH / 2
            self.stage.character.dx = 0

    def status_running(self):
        return self.running and not self.menu.has_quit

    def run_application(self):
        while self.status_running():
            event_list = pygame.event.get()
            if self.menu.is_enabled():
                self.menu.update(event_list)
            else:
                self.process_events(event_list)

            # Stage updates
            self.stage.update()
            self.stage.draw(self.render)

            # Debug draw
            if DEBUG:  # pragma: no cover
                pygame.draw.rect(
                    self.render,
                    (120, 0, 255),
                    self.stage.character.rect_collide,
                    2
                )
                for p in self.stage.platforms:
                    pygame.draw.rect(
                        self.render,
                        (255, 0, 0),
                        p.rect_collide,
                        2
                    )
                for i in self.stage.items:
                    pygame.draw.rect(
                        self.render,
                        (0, 255, 0),
                        i.rect_collide,
                        2
                    )

            self.menu.draw(self.render)

            # Collision detections
            item_hit = pygame.sprite.spritecollideany(
                self.stage.character,
                self.stage.items,
                collided=check_collision
            )

            if item_hit is not None:
                item_hit.update(hit=True)
                if isinstance(item_hit, StopLight):
                    if item_hit.state == StopLight.STATE_GREEN:
                        self.next_stage()

                elif isinstance(item_hit, Letter):
                    for item in self.stage.items:
                        if isinstance(item, StopLight):
                            item.state = StopLight.STATE_GREEN

            pygame.transform.scale(
                self.render,
                self.screen_mode,
                self.screen
            )
            pygame.display.update()
            self.clock.tick(FPS_DESIRED)


def main():
    if __name__ == "__main__":
        global DEBUG
        if len(sys.argv) > 1:  # pragma: no cover
            if "-d" in sys.argv:
                DEBUG = True
        app = Application()
        app.run_application()


main()
