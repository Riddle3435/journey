from pygame.sprite import LayeredDirty
import pygame


class Stage(object):
    def __init__(
            self,
            background,
            character=None,
            platforms=[],
            items=[]):
        self._background = pygame.image.load(background).convert()

        # Character
        self._char = character
        self._sprites = LayeredDirty()
        self._sprites.add(self._char)

        # Platforms
        self._platforms = LayeredDirty()
        for platform in platforms:
            self._platforms.add(platform)

        # Items
        self._items = LayeredDirty()
        for item in items:
            self._items.add(item)

    def update(self):
        self._sprites.update(self.platforms)
        self._items.update()

    def draw(self, surface):
        surface.blit(self._background, (0, 0))
        self._sprites.draw(surface)
        self._items.draw(surface)
        self._platforms.draw(surface)

    @property
    def items(self):
        return self._items

    @property
    def character(self):
        return self._char

    @property
    def platforms(self):
        return self._platforms
