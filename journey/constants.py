# General constants
RENDER_WIDTH = 800
RENDER_HEIGHT = 600
START_POS_X = 100
START_POS_Y = 450
FPS_DESIRED = 40

# Menu Constants
CREDITS = [
    'TEST MANIAC',
    'and lead dev',
    'Imara van Dinten',
    '',
    'RESIDENT VIM WIZARD',
    'and programming buddy',
    'Xander Bos',
    '',
    'ARTIST EXTREME',
    'and pixel magician',
    'ThePaintedDogg',
]
