from journey.constants import (
    START_POS_Y, START_POS_X,
    RENDER_WIDTH
)
from journey.character import Character
from journey.sprite import (
    CloudPlatform, Solid,
    Letter, StopLight,
    RedRobot, YellowRobot, CarRobot, DrunkenHorseRobot
)
from journey.stage import Stage


class Stages(object):
    BACKGROUND_1 = 'gfx/scene_1.png'
    BACKGROUND_2 = 'gfx/scene_2.png'
    BACKGROUND_3 = 'gfx/scene_3.png'
    BACKGROUND_4 = 'gfx/scene_4.png'

    LAST_CLOUD_1 = (350, 178)
    LAST_CLOUD_2 = (356, 270)
    LAST_CLOUD_3 = (555, 348)
    LAST_CLOUD_4 = (532, 305)

    POS_LETTER_1 = (LAST_CLOUD_1[0] + (CloudPlatform.WIDTH/2 - Letter.WIDTH/2),
                    LAST_CLOUD_1[1] - Letter.HEIGHT)
    POS_LETTER_2 = (LAST_CLOUD_2[0] + (CloudPlatform.WIDTH/2 - Letter.WIDTH/2),
                    LAST_CLOUD_2[1] - Letter.HEIGHT)
    POS_LETTER_3 = (LAST_CLOUD_3[0] + (CloudPlatform.WIDTH/2 - Letter.WIDTH/2),
                    LAST_CLOUD_3[1] - Letter.HEIGHT)
    POS_LETTER_4 = (LAST_CLOUD_4[0] + (CloudPlatform.WIDTH/2 - Letter.WIDTH/2),
                    LAST_CLOUD_4[1] - Letter.HEIGHT)

    def __init__(self):
        self.stages = []

    def get_stages(self):
        self.stages = [
            Stage(
                background=__class__.BACKGROUND_1,
                character=Character(START_POS_X, START_POS_Y),
                platforms=[
                    Solid(0, START_POS_Y + Character.HEIGHT,
                          RENDER_WIDTH, 100),
                    CloudPlatform(250, 430),
                    CloudPlatform(125, 367),
                    CloudPlatform(100, 304),
                    CloudPlatform(225, 241),
                    CloudPlatform(position=__class__.LAST_CLOUD_1)
                ],
                items=[
                    RedRobot(50, 600 - 80),
                    YellowRobot(150, 600 - 90),
                    Letter(position=__class__.POS_LETTER_1),
                    StopLight(
                        RENDER_WIDTH - 50,
                        START_POS_Y - StopLight.HEIGHT + Character.HEIGHT)
                ]),
            Stage(
                background=__class__.BACKGROUND_2,
                character=Character(START_POS_X, START_POS_Y),
                platforms=[
                    Solid(0, START_POS_Y + Character.HEIGHT,
                          RENDER_WIDTH, 100),
                    CloudPlatform(500, 450),
                    CloudPlatform(392, 388),
                    CloudPlatform(254, 330),
                    CloudPlatform(position=__class__.LAST_CLOUD_2)
                ],
                items=[
                    CarRobot(180, 600 - 80),
                    Letter(position=__class__.POS_LETTER_2),
                    StopLight(
                        RENDER_WIDTH - 50,
                        START_POS_Y - StopLight.HEIGHT + Character.HEIGHT)]
            ),
            Stage(
                background=__class__.BACKGROUND_3,
                character=Character(START_POS_X, START_POS_Y),
                platforms=[
                    Solid(0, START_POS_Y + Character.HEIGHT,
                          RENDER_WIDTH, 110),
                    CloudPlatform(270, 428),
                    CloudPlatform(360, 378),
                    CloudPlatform(450, 388),
                    CloudPlatform(position=__class__.LAST_CLOUD_3)
                ],
                items=[
                    DrunkenHorseRobot(220, 600 - 80),
                    Letter(position=__class__.POS_LETTER_3),
                    StopLight(
                        RENDER_WIDTH - 50,
                        START_POS_Y - StopLight.HEIGHT + Character.HEIGHT)]
            ),
            Stage(
                background=__class__.BACKGROUND_4,
                character=Character(START_POS_X, START_POS_Y),
                platforms=[
                    Solid(0, START_POS_Y + Character.HEIGHT,
                          RENDER_WIDTH, 110),
                    CloudPlatform(300, 430),
                    CloudPlatform(434, 370),
                    CloudPlatform(position=__class__.LAST_CLOUD_4)
                ],
                items=[
                    RedRobot(50, 600 - 80),
                    YellowRobot(150, 600 - 90),
                    CarRobot(300, 600 - 80),
                    DrunkenHorseRobot(500, 600 - 80),
                    Letter(position=__class__.POS_LETTER_4),
                    StopLight(
                        RENDER_WIDTH - 50,
                        START_POS_Y - StopLight.HEIGHT + Character.HEIGHT)]
            ),
        ]
        return self.stages
