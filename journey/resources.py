import pygame


class ResourceContainer(object):
    _instance = None
    _dict = {}

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ResourceContainer, cls).__new__(cls)
            cls._dict = {}
        return cls._instance

    def _get_from_dict(self, loader, filename):
        obj = self._dict.get(filename, None)
        if obj is None:
            self._dict[filename] = loader(filename)
            obj = self._dict[filename]
        return obj

    def get_image(self, filename):
        img = self._dict.get(filename, None)
        if img is None:
            self._dict[filename] = pygame.image.load(filename).convert_alpha()
            img = self._dict[filename]
        return img

    def get_sound(self, filename):
        return self._get_from_dict(pygame.mixer.Sound, filename)
