from collections import namedtuple
from journey.constants import RENDER_WIDTH, RENDER_HEIGHT
from journey.sprite import JourneySprite, check_collision
from pygame.locals import KEYDOWN, KEYUP, K_LEFT, K_RIGHT, K_SPACE


class Character(JourneySprite):
    State = namedtuple('CharacterState', ['name', 'row', 'delay', 'frames'])

    STANDING = State('Standing', 0, 7, 2)
    WALKING = State('Walking', 1, 5, 8)
    JUMPING = State('Jumping', 2, 5, 1)
    FALLING = State('Falling', 3, 2, 2)

    # Standard character-related constants
    HEIGHT = 64
    WIDTH = 32
    MASS = 1
    SPEED = 4
    VELOC = 7

    def __init__(self, x=0, y=0):
        JourneySprite.__init__(self, x, y, __class__.WIDTH, __class__.HEIGHT)
        self.image = 'gfx/character_sheet.png'
        # Update collision rectangle
        self.rect_collide.inflate_ip(
            -0.25*Character.WIDTH, -0.5*Character.HEIGHT)
        self.rect_collide.move_ip(0, 0.25*Character.HEIGHT)
        # Setup physics member variables
        self.airtime = 0
        self.grav_direction = 1
        self.velocity = 0
        # Miscellaneous member variables
        self.state = __class__.STANDING
        self.offset = 0

    def move(self, platforms):
        _dx, _dy = self.enforce_bounds()
        JourneySprite.move(self, _dx, _dy)
        collisions = [p for p in platforms
                      if check_collision(self, p)]
        for c in collisions:
            if _dy >= 0 and self.airtime >= 1:
                self.rect.bottom = c.rect_collide.top
                self.rect_collide.bottom = c.rect_collide.top
                # reset jump
                self.airtime = 0
                self.grav_direction = 1
                self.velocity = 0
                self.dy = 0

    def gravity(self):
        F = 0.5*__class__.MASS*self.grav_direction*(self.velocity**2)
        self.dy = round(-F)
        self.velocity -= 1
        self.airtime += 1
        if self.velocity < 0:
            self.grav_direction = -1

    def animate(self):
        self.source_rect.x = (self.frame_index * __class__.WIDTH) + self.offset
        self.source_rect.y = (self.state.row * __class__.HEIGHT)

        self.anim_ticks = (self.anim_ticks + 1) % self.state.delay
        if self.anim_ticks == 0:
            self.frame_index = (self.frame_index + 1) % self.state.frames

    def enforce_bounds(self):
        px, py = (self.rect.x + self.dx), (self.rect.y + self.dy)
        _dx, _dy = self.dx, self.dy
        if px < 0 or px > RENDER_WIDTH - __class__.WIDTH:
            _dx = 0
        if py < 0 or py > RENDER_HEIGHT - __class__.HEIGHT:
            _dy = 0
        return _dx, _dy

    def update(self, platforms):
        self.next_state()
        self.animate()
        self.gravity()
        self.move(platforms)

    def next_state(self):
        # Assume the next state is the current state
        next_state = self.state
        if self.state == Character.STANDING:
            if self.dy == 0:
                if self.dx != 0:
                    next_state = Character.WALKING
            elif self.dy < 0:
                next_state = Character.JUMPING
            else:
                next_state = Character.FALLING
        elif self.state == Character.WALKING:
            if self.dy == 0:
                if self.dx == 0:
                    next_state = Character.STANDING
            elif self.dy < 0:
                next_state = Character.JUMPING
            else:
                next_state = Character.FALLING
        elif self.state == Character.JUMPING:
            if self.dy > 0:
                next_state = Character.FALLING
        else:
            if self.dy == 0:
                if self.dx != 0:
                    next_state = Character.WALKING
                else:
                    next_state = Character.STANDING
        # If the next state is different, reset the frame counter
        if next_state != self.state:
            self.frame_index = 0
        self.state = next_state

    def key_handler(self, event):
        if event.type == KEYDOWN:
            if event.key == K_LEFT:
                self.dx = -__class__.SPEED
                self.offset = 8 * __class__.WIDTH
            elif event.key == K_RIGHT:
                self.dx = __class__.SPEED
                self.offset = 0
            elif event.key == K_SPACE:
                if self.airtime < 6:
                    self.grav_direction = 1
                    self.velocity = __class__.VELOC
            else:
                return
        elif event.type == KEYUP:
            if event.key == K_LEFT or event.key == K_RIGHT:
                self.dx = 0
        else:
            return
