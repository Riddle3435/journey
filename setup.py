from cx_Freeze import setup, Executable
import sys

build_exe_options = {
    'packages': ['pygame', 'pygame_menu'],
    'excludes': [],
    'include_files': [
        ('gfx/character_sheet.png', 'gfx/character_sheet.png'),
        ('gfx/clouds.png', 'gfx/clouds.png'),
        ('gfx/icon.png', 'gfx/icon.png'),
        ('gfx/letter.png', 'gfx/letter.png'),
        ('gfx/menubar.png', 'gfx/menubar.png'),
        ('gfx/robots.png', 'gfx/robots.png'),
        ('gfx/scene_1.png', 'gfx/scene_1.png'),
        ('gfx/scene_2.png', 'gfx/scene_2.png'),
        ('gfx/scene_3.png', 'gfx/scene_3.png'),
        ('gfx/scene_4.png', 'gfx/scene_4.png'),
        ('gfx/stoplight.png', 'gfx/stoplight.png'),
        ('sfx/paper.ogg', 'sfx/paper.ogg')
    ]
}

base = None
icon = 'gfx/icon.png'
if sys.platform == 'win32':
    base = 'Win32GUI'
    icon = 'gfx/icon.ico'

setup(
    name='Journey',
    version='1.0.0',
    description='A platformer game that illustrates my journey.',
    options={'build_exe': build_exe_options},
    executables=[
       Executable(
            'journey/main.py',
           icon=icon,
           base=base,
           targetName='Journey'
       )
    ]
)
